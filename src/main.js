import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import MyInput from '@/components/ui/my-input.vue'

Vue.config.productionTip = false

Vue.component('my-input', MyInput)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
